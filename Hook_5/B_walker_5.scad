$fn = 50;
M4 = true;
M6 = false;
ROD6 = false;

o = 1;   // rounding offset

a = 30;      // parallelogram angle
w8 = 8;      // square rod width
w6 = 6;      // square rod width
m4r = 2;
  m6r = 3;     // screw hole radius
m5r = 5/2.0; // screw radius

w = ROD6 ? w6 : w8;
r = M4 ? m4r : M6 ? m6r : m5r;

// all values are without rounding (camfering)
pH  = 3*r;    // parallelogram vertical height
pW  = 3*w;    // parallelogram horizontal width

x0 = 0;          // point 0 x coord
x1 = pW;         // point 1 x coord
x2 = x1;         // point 2 x coord
//x3 = x1/2.0;   // NO computed below ; point 3 x coord
x4 = x0+ 2*r;    // point 4 x coord
x5 = x0;         // point 5 x coord
y0 = 0;          // point 0 y coord
y1 = x1*tan(a);  // point 1 y coord
y5 = pH/cos(a);  // point 5 y coord
y2 = y1 +y5;     // point 2 y coord
//y3 = (y2+y5)/2.; // No! computed below point 3 y coord
y4 = y2;         // point 4 y coord

pBy = y1 + 2*o/cos(a);  //parallelolgram vertical base length with rounding
pBx  = x1/cos(a) + 2*o; // parallelogram horiz base length with rounding
pZ = 3*w;               // object Z extrusion height

squeezeZ = w*0.75; 

// extra space needed for physical parts to fit
rodEpsilon = 0.1;
nutREpsilon = 0.05;

function f2f2R(f2f) =  f2f/(2*sin(60));

m4nutZ = 3.2;  // normal nut
m4nutFlat2Flat = 7;
m4nutR = f2f2R(m4nutFlat2Flat);

m5nutZ = 2.7; // jam nut
m5nutFlat2Flat = 8;
m5nutR = f2f2R(m5nutFlat2Flat);
m6nutZ = 3.2;  // jam nut
m6nutFlat2Flat = 10;
m6nutR = f2f2R(m6nutFlat2Flat);

nutR = M4 ? m4nutR : M6 ? m6nutR : m5nutR;
nutZ = M4 ? m4nutZ : M6 ? m6nutZ : m5nutZ;

echo(m5nutR);
echo(m6nutR);


circleCenterA = [x4, tan(a)*x1/4. + y5/2.,0];
circleCenterB = [x4, y4/2.,0];

circleCenter = circleCenterA;

pPoints= [[x0,y0],
          [x1,y1],
          [x2,y2],
          //[x3,y3],
          getXY3(x4,x5,y4,y5,a),
          [x4,y4],
          [x5,y5]];
          
          
function getXY3(X4,X5,Y4,Y5,alpha) =
  let(Delta = X4-X5,
      Phi   = Delta*tan(alpha),
      L   = Y4-Phi-Y5,
      Zeta = L*cos(alpha),
      Psi = Zeta*sin(alpha),
      Theta = Zeta*cos(alpha))
      [X4+Psi, Y4-Theta];

module flatShape(){
  offset(o)
    polygon(pPoints);
}
module flatRod(){
  //translate([pW-w,0,0])
    square([w+rodEpsilon,100], center = true);
}
module flatCircle(e){
//  translate(circleCenter)
    circle(r=r+e);
}
module flatNut(e){
  rotate([0,0,30])
    circle(r=nutR+e,$fn=6);
}

module flatSqueezeCut(){
  square([4*x1/3,70,],center=true);
}

module flatAll(){
    difference(){
      flatShape();
      translate([pW-w,0,0])
        flatRod();
      translate(circleCenter){
        flatCircle(nutREpsilon);
        flatNut(nutREpsilon);
      }
    }
}
module flatAllCut(){
  difference(){
    flatSqueezeCut();
    flatAll();
    translate(circleCenter)
      flatCircle(nutREpsilon);
  }
}

module flatALLOk(){
  flatAll();
  translate(circleCenter)
    flatCircle(nutREpsilon);
}

module all3D(){
  difference(){
    linear_extrude(height=pZ)
      flatShape();
    linear_extrude(height=nutZ,center=true)
      translate(circleCenter)
        flatNut(nutREpsilon);
    linear_extrude(height=100,center=true)
      translate(circleCenter)
        flatCircle(nutREpsilon);
    translate([0,0,pZ*0.5]){
      linear_extrude(height=squeezeZ,center=true)
        flatSqueezeCut();
      linear_extrude(height=w+rodEpsilon,center=true)
        translate([pW-w,0,0])
          flatRod();
    }
  }
}

module allUpright3D(){
  rotate([90,0,0])
    rotate([0,0,-30])
      all3D();
}

allUpright3D();
