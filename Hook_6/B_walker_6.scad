$fn = 50;
M3 = 3;
M4 = 4;
M5 = 5;
M6 = 6;
M8 = 8;
// angle related
a = 30; // alpha
s = sin(a);
c = cos(a);
t = tan(a);

/* A fully parametric version
 * Paramters are:
 * Rod Width
 * Rod type (round or square)
 * Bolt Diameter 
 * Nut Hold Base space : (min space outside nut 
 *     to maintain it in position, could be zero)
 * nut epsilon to comensate nut hole reduction during print
 * rod epsilon to comensate rod hole reduction during print
 */

/* Info
 * M3 Flat2Flat = 5.5
 * M3 Height    = 2.4
 * M4 Flat2Flat = 7
 * M4 Height    = 3.2
 * M5 Flat2Flat = 8
 * M5 Height    = 4
 * M6 Flat2Flat = 10
 * M6 Height    = 5
 */
 
// Primary Parameters //
Bolt  = M4;
Rod   = M8;
RodSq = true;

// Secondary Parameters //
RodEpsilon = 0.1; // added to width or diameter, not radius
NutHoldB   = 0;  //border around the nut added to flat2flat/2
NutEpsilon = 0.5; // radius measurement, not flat to flat
o          = 1;   // rounding offset

/*************** End of Parameters ***************/
// Derived values
 
// W = rod width
W  = Rod;
// N = nut flat to flat+ expansion space
N   = (2*c*NutEpsilon) + (Bolt==M3 ? 5.5 : 
                          Bolt==M4 ? 7   : 
                          Bolt==M5 ? 8   : 
                          Bolt==M6 ? 10  : -1);
// NH = nut height
NH  = Bolt==M3 ? 2.4 : 
      Bolt==M4 ? 3.2 : 
      Bolt==M5 ? 4   : 
      Bolt==M6 ? 5 : -1;

// D = circle Diameter
D   = Bolt + RodEpsilon;

echo ("W",W);
echo("N",N);
echo("NH",NH);
echo("D",D);

NutHold = NutHoldB - o;

// dimension related
H = N + 2*NutHold;  //perpendicular height of parallelogram
L = 1.5 * W + 2*(N*t +NutHold);  // perpendicular length of parallelogram
NR = N*0.5/c;   // nut radius
echo("H",H);
echo("L",L);
echo("NR",NR);

// all point coord values are without rounding (camfering)

// simplification values
h0  =  L*t;
h1 = H/c;

x0 = 0;          // point 0 coord
y0 = 0;
x1 = L;          // point 1  coord
y1 = h0;
x2 = x1;         // point 2 coord
y2 = h0 + h1;
x3 = x2 -1.5*W;  // point 3  coord
y3 = y2 -1.5*W*t;
x4 = x3 + (y3-y2)*t;  // point 4 coord
y4 = y2;
x5 = x0;         // point 5 coord
y5 = h1;          

// circle coords --RodEpsilon*0.1 to ensure overlap
xc = N*0.5 + NutHold -RodEpsilon*0.1;
yc = xc*t + (0.5*N+NutHold)/c -RodEpsilon*0.1;
echo("xc",xc);

circleCenter =  [xc,yc];

// rod X coord
xr =  L - W;
echo("xr",xr);

// width of cut out for squeez
squeezeZ = W*0.75;

// Z dimension of part
PZ = 2*W;

pPoints= [[x0,y0],
          [x1,y1],
          [x2,y2],
          [x3,y3],
          [x4,y4],
          [x5,y5]];
          
          
module flatShape(){
  offset(o)
    polygon(pPoints);
}
module flatAll(){
  difference(){
    flatShape();
    translate(circleCenter)
        flatNut();
    translate([xr,0,0])
      flatRod();
  }
  translate([xc,yc,0])
      flatCircle();
}
module flatRod(){
    square([W+RodEpsilon,100], center = true);
}
module flatCircle(){
    circle(d=D);
}
module flatNut(){
  rotate([0,0,30])
    circle(r=NR,$fn=6);
}

module flatSqueezeCut(){
  square([4*x1/3,70,],center=true);
}
module flatAllCut(){
  difference(){
    flatSqueezeCut();
    flatAll();
    translate(circleCenter)
      flatCircle();
  }
}

module flatAllOk(){
  flatAll();
  translate(circleCenter)
    flatCircle();
}

module roundRod(){
  translate([xr,0,0])
    rotate([90,0,0])
      cylinder(d=W+RodEpsilon,h=100,center=true);
}

module all3D(){
  difference(){
    linear_extrude(height=PZ)
      flatShape();
    linear_extrude(height=NH,center=true)
      translate(circleCenter)
        flatNut();
    linear_extrude(height=100,center=true)
      translate(circleCenter)
        flatCircle();
    translate([0,0,PZ*0.5]){
      linear_extrude(height=squeezeZ,center=true)
        flatSqueezeCut();
      if (RodSq)
        linear_extrude(height=W+RodEpsilon,center=true)
          translate([xr,0,0])
            flatRod();
      else
        roundRod();
    }
  }
}
module allUpright3D(){
  rotate([90,0,0])
    rotate([0,0,-30])
      all3D();
}

///all3D();


allUpright3D();
