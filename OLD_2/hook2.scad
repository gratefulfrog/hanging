$fn        = 50;

function in2mm(in) = in*25.4;

nutThinZ = in2mm(5/32);
nutZ = in2mm(0.226);
nutR = in2mm(0.438)/2*sin(60);
rodW     = in2mm(0.25);
rodZ     = 200;
nutFlat2Flat = in2mm(.438);

holeEpsilon = 0.5;

extOffset  =  1.5;          
ext2Offset =  2*extOffset;

blockZ = 50;
blockX = 3*(rodW+holeEpsilon);
blockY = 4*(rodW+holeEpsilon);

slotZ = 15;
slotBaseExt = 2;

foldY = 2;

quarterInchBoltDia = in2mm(0.25);
quarterInchBoltHoleDia = quarterInchBoltDia  + holeEpsilon;
echo(nutZ);
echo(nutThinZ);
echo(nutR);
echo(rodW+holeEpsilon);
echo(quarterInchBoltHoleDia );
echo(nutFlat2Flat);

blockPoints =  [[0,0],
                [blockY - ext2Offset,0],
                [blockY - ext2Offset,blockZ - ext2Offset],
                [rodW+holeEpsilon+slotBaseExt,blockZ - ext2Offset],
                [rodW+holeEpsilon+slotBaseExt,slotZ],
                [rodW+holeEpsilon-slotBaseExt,slotZ],      
                [0,blockZ - ext2Offset]];

rodHolePoints= [[0,0],
                [rodW,0],
                [rodW+holeEpsilon/2.0,rodW+holeEpsilon],
                [-holeEpsilon/2.0,rodW+holeEpsilon]];
                
module trapezoidRod(h){
  translate([0,0,-h/2.])
    linear_extrude(height = h)
      translate([-(rodW+holeEpsilon)/2.0,rodW/2.0,0])
        rotate([0,0,-90])
          polygon(rodHolePoints);
}

module shapedBlock(){
  rotate([90,0,0])
    linear_extrude(height=blockX)
      translate([extOffset,extOffset])
        offset(r=extOffset)
          polygon(blockPoints);
}

module quarterInchNut(e){
  rotate([90,0,0])
    cylinder(r=nutR+e,h=nutThinZ+e,$fn=6, center = true);
}

module nuttyThin(n=1){
  for (i=[0:0.25:n])
    translate([2.5*(rodW+holeEpsilon)+i*nutFlat2Flat,
              nutThinZ-rodW-0.1,
              2*blockZ/3])
      rotate([90,0,0])
        quarterInchNut(holeEpsilon);
}
module quarterInchNutTest(){
  quarterInchNut(0);
  color("Magenta",0.3)
    quarterInchNut(holeEpsilon);
}
module rod(e){
  x = rodW+e;
  y = rodW+e;
  z = rodZ+e;
  cube([x,y,z],center = true);
} 

module rodTest(){
  rod(0);
  color("Lime",0.3)
    rod(holeEpsilon);
}

module block(){
  x = blockX;
  y = blockY;
  z = blockZ;
  cube([x,y,z],center=true);
}
module shapedBlockRod(){
  difference(){
    shapedBlock();
    translate([0,rodW+holeEpsilon,0])
      rod(holeEpsilon);
  }
}

module rounder(){
  translate([rodW+holeEpsilon+slotBaseExt*0.7,0,slotZ+1.5*slotBaseExt])
    rotate([90,0,0])
      cylinder(d=slotBaseExt*0.6,h= 50,center = true);
}
module doit(){
  difference(){
    shapedBlock();
      translate([2.5*(rodW+holeEpsilon),-1.5*(rodW+holeEpsilon),0])
        trapezoidRod(2*blockZ);
      rounder();
      slot(3*blockZ);
      translate([1.2*(rodW+holeEpsilon),0,1.6*blockZ/9]){
        quarterInchNut(holeEpsilon);
        rotate([90,0,0])
          cylinder(d=quarterInchBoltHoleDia, h=100,center =true);
      }
  }
}

module slot(h){
  translate([0,-blockX/2.,0])
    cube([1.5*blockX,foldY,h], center = true);
}
 

doit();

