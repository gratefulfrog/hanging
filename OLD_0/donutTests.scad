$fn=30;

hullRad = 1.5;
vAngle   = 20;
cH       = 40;


module donut(hookAngle){
  //translate([0,0,hullRad])
  rotate([-90,hookAngle/2.,0])
    rotate_extrude (angle = 180-hookAngle)
      translate([1*hullRad,0,0])
        circle(r=hullRad);
}
module angleCyl(h,a){
  ratio = a == 0 ? 1 : abs(a)/a;
    translate([0,0,0])
      rotate([0,a/2.,0])
        translate([hullRad*ratio,0,0]){
          cylinder(h=h,r=hullRad);
          translate([0,0,h])
            sphere(r=hullRad);
        }
}
module V(angle,cylH){
  rotate([0,angle/2.,0])
  translate([hullRad,0,0]){
    donut(angle);
    angleCyl(cylH,-angle);
    angleCyl(cylH,angle);
  }
}

module all(hM){
  translate([0,hM,0])
     V(vAngle,cH);
  V(vAngle,cH);

  rotate([-90,0,0])
      linear_extrude(height=hM)
        projection(cut=true)
          rotate([90,0,0])
            V(vAngle,cH);
}
all(20);