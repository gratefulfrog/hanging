$fn        = 50;


hullRad  = 1.5;

rodW       = 25.4/4;
rodEpsilon = 0.5;
rodWCut    = rodW + rodEpsilon;
rodH       = 50;
rodHCut    = rodH + rodEpsilon+hullRad*2.;

widthMultiplier = 2.5;
gripperStandOff =  5;

screwDia        = 3.2;

gripperW = rodWCut*widthMultiplier;
gripperL = gripperW + gripperStandOff;
gripperH = rodH;

hookDepth = gripperH/3.0;

module rod(){
  cube([rodWCut, rodWCut, rodHCut],center = true);
}

module screwHole(h){
  translate([-rodH/2.,0,h])
    rotate([0,90,0])
      cylinder(d=screwDia,h=rodH);
}
/*
module gripper(){
  translate([0, (gripperW-gripperL)/2.,0]){
    cube([gripperW,gripperL,gripperH],center=true);
  }
}
*/
module hullSphere(x,y,z){
  translate([x,y,z])
    sphere(hullRad);
}

module all(){
  difference(){
    gripper();  
    scale([1,1,10])
      rod();
    screwHole(-gripperH/6.);
    screwHole(gripperH/6.);
  }
}
module gripperBlock(){
  hull()
    for (i=[-1,1])
     for (j=[-1,1])
      for (k=[-1,1]) 
        hullSphere(gripperW/2.0*i,gripperL/2.0*j,gripperH/2.0*k);
}
module hookCut(){
  hull(){
    translate([-rodHCut/2.0,0,hookDepth])
      rotate([0,90,0])
        cylinder(r=hullRad*1, h= rodHCut);
    translate([-rodHCut/2.0,0,0])
      rotate([0,90,0])
        cylinder(r=hullRad*1, h= rodHCut);
  }
}
  
module all(){
  difference(){
    gripperBlock();
     translate([0,-gripperL/2.0+2*hullRad+2,10]){
     hookCut();
    }
    translate([0, -(gripperW-gripperL)/2.,0]){
      rod();
      #screwHole(-gripperH/4.);
      #nutHole(-gripperH/4.);
      #screwHole(gripperH/4.);
      #nutHole(gripperH/4.);
    }
  }
}

module nutHole(h){
 translate([0,0,h])
  rotate([0,90,0])
    cylinder(r=2*sqrt(3.)+0.4,h= 2*3.5+rodWCut, center=true,$fn=6);
}

module outerHull(){
  hull(){
  hullSphere(gripperW/2.0,
             (-gripperL/2.0+2*hullRad+2)-(hullRad*1.5),
             gripperH/2.0);
  hullSphere(-gripperW/2.0,
             (-gripperL/2.0+2*hullRad+2)-(hullRad*1.5),
             gripperH/2.0);
  hullSphere(gripperW/2.0,
             (-gripperL/2.0+2*hullRad+2)-(hullRad*1.5),
             gripperH/2.0-hookDepth+6);
  hullSphere(-gripperW/2.0,
             (-gripperL/2.0+2*hullRad+2)-(hullRad*1.5),
             gripperH/2.0-hookDepth+6);
  }
}

module demiDonut(){
  rotate([-90,0,90])
    rotate_extrude (angle = 180)
      translate([2*hullRad,0,0])
        circle(r=hullRad);
}
module slotWalls0(){
  difference(){
    union(){
      //all();
  translate([0,4*hullRad,0])
    outerHull();
  outerHull();
  hull(){
  translate([gripperW/2.0,
             (-gripperL/2.0+2*hullRad+2)-(hullRad*1.5)+2*hullRad,
             gripperH/2.0-hookDepth+6])
    demiDonut();
  translate([-gripperW/2.0,
             (-gripperL/2.0+2*hullRad+2)-(hullRad*1.5)+2*hullRad,
             gripperH/2.0-hookDepth+6])
    demiDonut();
  }
  }
  translate([0,-3.5*hullRad,gripperH/2.0-hookDepth+6])
  hookCut();
  }
}
donutX = gripperW/2.0;
module demiDonuts(){
  for (i=[-1,1])
    translate([i*donutX,0,0])
      demiDonut();
}

module hullWall(){
  hull(){
    for (z=[0,hookDepth])
      for (i=[1,-1])
        hullSphere(i*donutX,0,z);

    }
  }
module hullWalls(){
    for (y=[-2,2])
      translate([0,y*hullRad,0])
        hullWall();
  }
  
module lowCutCyl(){
rotate([0,90,0])
  cylinder(r=hullRad,h=(donutX+hullRad)*2,center=true);
}

//intersection(){

module donutHullTrimmers(){
  translate([-donutX,0,0])
    rotate([0,0,180])
      donutHullTrimmer();
  translate([donutX,0,0])
    donutHullTrimmer();
}

module donutHullTrimmer(){
  difference(){
    hull() 
      demiDonut();
    demiDonut();
    translate([-5,0,0])
    cube([10,10,10],center = true);
  }
}

module niceHook(){
  difference(){
    hull(){
      demiDonuts();  
    }
    donutHullTrimmers();
    lowCutCyl();
    }
  hullWalls();
}

all();