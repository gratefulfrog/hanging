$fn = 50;

o = 1.5;   // offset
a = 30;  // parallelogram angle
w = 8;  // square rod width
x = 4*w;
r = 3;  // crew hole radius
h = 4*r-2*o;
y =  h/cos(a);
z = 3*w;
e = 1;
cirCenter = [w,w*tan(a)+ 2*r/cos(a),0];

function in2mm(in) = in*25.4;
nutThinZ = in2mm(5/32);
nutR = in2mm(0.438)/2*sin(60);

pointsP = [[0,0],
           [x, x*tan(a)],
           [x, x*tan(a) + h/cos(a)],
           [2*w,2*w*tan(a) + y],
           [w, 2*w*tan(a) + y + 2*r/cos(a)],
           [0, y]];

pointsT = [[0, y],
           [2*w,2*w*tan(a) + y],
           [w, 2*w*tan(a) + y + 2*r/cos(a)]];
  
module shape(){
  //translate([o,o,0])
  linear_extrude(height= z)  
    difference(){
      offset(o)
        polygon(pointsP);
      translate(cirCenter)
        circle(r=r);
    }
}

module rod(e){
  cube([e+w,100,e+w], center = true);
}

module nut(e){
  rotate([0,90,0])
    cylinder(r=nutR,h=nutThinZ,$fn=6, center = true);
}

difference(){
  shape();
  translate([3*w,0,z/2.])
    rod(e);
  translate([0,0,z/2.])
    squeezeCut();
  translate(cirCenter)
    rotate([0,90,30])
      nut();
  
}



module squeezeCut(){
  cube([2*3*w,100,0.75*w],center=true);
}
