$fn = 50;
M3 = 3;
M4 = 4;
M5 = 5;
M6 = 6;
M8 = 8;
// angle related
a = 30; // alpha
s = sin(a);
c = cos(a);
t = tan(a);

/* A fully parametric version
 * Paramters are:
 * Rod Width
 * RodSq true if square rod, false implies round rod 
 * Bolt Diameter 
 * rod epsilon to comensate rod hole reduction during print
 * Nut Hold space : (min space outside nut 
 *     to maintain it in position, could be zero)
 * nut epsilon to comensate nut hole reduction during print
 *
 */

/* Info
 * M3 Flat2Flat = 5.5
 * M3 Height    = 2.4
 * M4 Flat2Flat = 7
 * M4 Height    = 3.2
 * M5 Flat2Flat = 8
 * M5 Height    = 4
 * M6 Flat2Flat = 10
 * M6 Height    = 5
 */
 
// Primary Parameters //
Bolt  = M4;
Rod   = M6;
RodSq = true;

// Secondary Parameters //
RodEpsilon = 0.1; // added to width or diameter, not radius
NutHold    = 0;   //border around the nut added to flat2flat/2
NutEpsilon = 0.1; // radius measurement, not flat to flat
o          = 1;   // rounding offset
/////////////////////////////////////////
MinArmThickness = 4;  // thickness of arms
MinNutDepth     = 1.5;  // nut countersink depth

/*************** End of Parameters ***************/
// Derived values
 
// W = rod width
W  = Rod;
// N = nut flat to flat+ expansion space
N   = (2*c*NutEpsilon) + (Bolt==M3 ? 5.5 : 
                          Bolt==M4 ? 7   : 
                          Bolt==M5 ? 8   : 
                          Bolt==M6 ? 10  : -1);
// NH = nut height
NH  = Bolt==M3 ? 2.4 : 
      Bolt==M4 ? 3.2 : 
      Bolt==M5 ? 4   : 
      Bolt==M6 ? 5 : -1;

// D = circle Diameter
D   = Bolt + RodEpsilon;

echo ("W",W);
echo("N",N);
echo("NH",NH);
echo("D",D);

// dimension related
H = N + 2*NutHold - 2*o;  //perpendicular height of parallelogram
L = MinArmThickness + W + N + 2*NutHold -2*o;  // perpendicular length of parallelogram
NR = N*0.5/c;   // nut radius
echo("H",H);
echo("L",L);
echo("NutHold", NutHold);
echo("NR",NR);

// circle coords --RodEpsilon*0.1 to ensure overlap
xc = N*0.5 + NutHold-o - RodEpsilon*0.1;
yc = xc*t + (0.5*N+NutHold-o)/c -RodEpsilon*0.1/c;
echo("xc",xc);

circleCenter =  [xc,yc];

// rod X coord x axis
xr =   xc + 0.5*N +NutHold +0.5*W; //L - 0.5*W - MinArmThickness;
echo("xr",xr);

// all point coord values are without rounding (camfering)

// simplification values
h0  =  (xr + 0.5*W + MinArmThickness-o)*t;
h1 = H/c;

x0 = 0;          // point 0 coord
y0 = 0;
x1 = xr + 0.5*W + MinArmThickness-o;        // point 1  coord
y1 = h0;
x2 = x1;         // point 2 coord
y2 = h0 + h1;
x3 = xr -0.5*W;  // point 3  coord
y3 = y2 -(W+MinArmThickness-o)*t ;
x4 = x3 + (y3-y2)*t;  // point 4 coord
y4 = y2;
x5 = x0;         // point 5 coord
y5 = h1;          

// width of cut out for squeez
squeezeZ = W*0.75;

// Z dimension of part
PZ = W+ 2*MinArmThickness;

pPoints= [[x0,y0],
          [x1,y1],
          [x2,y2],
          [x3,y3],
          [x4,y4],
          [x5,y5]];
          
          
module flatShape(){
  offset(o)
    polygon(pPoints);
}
module flatAll(){
  difference(){
    flatShape();
    translate(circleCenter)
        flatNut();
    translate([xr,0,0])
      flatRod();
  }
  translate([xc,yc,0])
      flatCircle();
}
module flatRod(){
    square([W+RodEpsilon,100], center = true);
}
module flatCircle(){
    circle(d=D);
}
module flatNut(){
  rotate([0,0,30])
    circle(r=NR,$fn=6);
}

module flatSqueezeCut(){
  square([4*x1/3,70,],center=true);
}
module flatAllCut(){
  difference(){
    flatSqueezeCut();
    flatAll();
    translate(circleCenter)
      flatCircle();
  }
}

module flatAllOk(){
  flatAll();
  translate(circleCenter)
    flatCircle();
}

/*
translate([xr,0,0])
      flatRod();

rotate([0,0,0])
translate([0,-10])
flatAllOk();
*/
module roundRod(){
  translate([xr,0,0])
    rotate([90,0,0])
      cylinder(d=W+RodEpsilon,h=100,center=true);
}

module all3D(){
  difference(){
    linear_extrude(height=PZ)
      flatShape();
    linear_extrude(height=MinNutDepth*2,center=true)
      translate(circleCenter)
        flatNut();
    linear_extrude(height=100,center=true)
      translate(circleCenter)
        flatCircle();
    translate([0,0,PZ*0.5]){
      linear_extrude(height=squeezeZ,center=true)
        flatSqueezeCut();
      if (RodSq)
        linear_extrude(height=W+RodEpsilon,center=true)
          translate([xr,0,0])
            flatRod();
      else
        roundRod();
    }
  }
}
module allUpright3D(){
  rotate([90,0,180])
    rotate([0,0,-30])
      all3D();
}

//all3D();


allUpright3D();
