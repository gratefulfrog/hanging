$fn        = 50;

function in2mm(in) = in*25.4;

nutThinZ = in2mm(5/32);
nutZ = in2mm(0.226);
nutR = in2mm(0.438)/2*sin(60);
rodW     = in2mm(0.25);
rodZ     = 200;
nutFlat2Flat = in2mm(.438);

holeEpsilon = 0.5;

extOffset  =  1.5;          
ext2Offset =  2*extOffset;

blockZ = 50;
blockX = 3*(rodW+holeEpsilon);
blockY = 4*(rodW+holeEpsilon);
/*
slotZ = 15;
slotBaseExt = 2;
*/
foldY = 2;

quarterInchBoltDia = in2mm(0.25);
quarterInchBoltHoleDia = quarterInchBoltDia  + holeEpsilon;

echo(nutZ);
echo(nutThinZ);
echo(nutR);
echo(rodW+holeEpsilon);
echo(quarterInchBoltHoleDia );
echo(nutFlat2Flat);


module shapedBlock(){
  rotate([90,0,0])
    linear_extrude(height=blockX)
      translate([extOffset,extOffset])
        offset(r=extOffset)
          polygon(blockPoints);
}

module quarterInchNut(e){
  rotate([0,90,0])
    cylinder(r=nutR+e,h=nutThinZ+e,$fn=6, center = true);
}

module nuttyThin(n=1){
  for (i=[0:0.25:n])
    translate([2.5*(rodW+holeEpsilon)+i*nutFlat2Flat,
              nutThinZ-rodW-0.1,
              2*blockZ/3])
      rotate([90,0,0])
        quarterInchNut(holeEpsilon);
}
module quarterInchNutTest(){
  quarterInchNut(0);
  color("Magenta",0.3)
    quarterInchNut(holeEpsilon);
}
module rod(e){
  x = rodW+e;
  y = rodW+e;
  z = rodZ+e;
  cube([x,y,z],center = true);
} 
cylD = 2*rodW*sqrt(2);
cylZ= nutR*3;
armZ = cylZ;
armX = cylD;
armY = 2.*cylZ;

module arm(){
  translate([0,armY/2.,0])
    cube([armX,armY,armZ], center=true);
}
module extCyl(){
    cylinder(d=cylD, h= cylZ,center=true);
}
module squeezeSlot(){
  translate([0,cylD+rodW,0])  
    cube([rodW*3/4.0,3*cylZ,cylZ*10], center=true);
}
module boltHole(){
  translate([0,armY*3/4,0])
    rotate([0,90,0])
      cylinder(d=quarterInchBoltHoleDia,h= 4*armX,center=true);
}
module nutHole(){
  translate([armX/2.,armY*3/4,0])
    quarterInchNut(0);
}
module roller(){
  rotate([0,90,0])
    cylinder(d=armY/2,h= armX,center=true);
}
module hook(){
  difference(){
    union(){
      arm();
      extCyl();
      translate([0,3*armY/4.,armZ/2.])
        roller();
      translate([0,1*armY/4.,armZ/2.])
        roller();
    }
    rod(0);
    squeezeSlot();
    boltHole();
    nutHole();
  }
}
hook();

